module.exports = {
  db: {
    host: 'mongodb://localhost:27017/',
    database: 'game',
    userTable: 'users',
    // messageTable: 'messages',
    // commandTable: 'commands',
    // actionTable: 'actions',
    logTable: 'logs'
  },
  serverURL: 'http://localhost:3000/',
  // secret: 'thisisasecretpassword',
  clientURL: 'http://localhost:3001'
};
