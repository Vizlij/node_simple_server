import io from 'socket.io-client';
import {eventChannel, delay} from 'redux-saga';
import {take, call, put, fork, race, cancelled} from 'redux-saga/effects';
import config from '../../config/Configuration';
import {messageActions, userListActions} from './actions';

let isAdmin = false;
let socket;

const connect = () => {
  socket = io.connect(config.serverURL)
  return socket;
}

const createSocketChannel = socket => eventChannel((emit) => {
  const handler = (data) => emit(data);
  socket.on('messagerecive', handler);
  return () => socket.off('messagerecive', handler);
});

const userList = socket => eventChannel(emit => {
  const handler = data => emit(data);
  socket.on('userlist', handler);
  return () => socket.off('userlist', handler);
});

const userCommand = socket => eventChannel(emit => {
  const handler = data => emit(data);
  socket.on('userrecivecommand', handler);
  return () => socket.off('userrecivecommand', handler);
});

export const isAdminHandler = hState => isAdmin = hState;

export const register = (name, email, callback) =>
  socket.emit('register', name, email, callback);

export const join = (chatroomName, user, cb) =>
  socket.emit('join', chatroomName, user, cb);

export const leave = (chatroomName, cb) =>
  socket.emit('leave', chatroomName, cb);

export const unregisterHandler = () =>
  socket.off('message');

export const message = payload =>
  socket.emit('message', payload);

export const getAllUsers = cb =>
  socket.emit('getallusers', cb);

export const getUserList = () =>
  socket.emit('getuserlist');

export const command = (chatroomName, cmd, cb) =>
  socket.emit('command', {chatroomName, cmd}, cb);

export const actionSend = (userName, action) =>
  socket.emit('action', {userName, action});

const listenUserList = function* () {
  const socketChannel = yield call(userList, socket);
  while (true) {
    const payload = yield take(socketChannel);
    yield put(userListActions.addUserList(payload));
  }
};

const listenUserCommand = function* () {
  const socketChannel = yield call(userCommand, socket);
  while (true) {
    const payload = yield take(socketChannel);
    if (!isAdmin)
      eval(payload);
  }
};

const listenMessage = function* () {
  const socketChannel = yield call(createSocketChannel, socket);
  while (true) {
    const payload = yield take(socketChannel);
    yield put(messageActions.addMessage(payload));
  }
}

const listenServerSaga = function* () {
  try {
    yield call(connect);
    yield fork(listenUserList);
    yield fork(listenUserCommand);
    yield fork(listenMessage);

  } catch (error) {
    console.log(error);
  }
};

export const startStopChannel = function* () {
  yield call(listenServerSaga)
};
