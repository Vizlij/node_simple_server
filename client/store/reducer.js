import {combineReducers} from 'redux';
import { userReducer, messageReducer, userListReducer } from './reducers/index';

const rootReducer = combineReducers({
  userReducer,
  messageReducer,
  userListReducer
});

export default rootReducer;
