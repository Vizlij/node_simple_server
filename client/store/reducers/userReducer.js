import { userConstants } from '../../constants';

const initialState = {
  auth: false,
  id: null,
  token: null,
  error: null,
  role: null,
  name: null,
  statistic: []
};

export const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case userConstants.USER_LOGIN_SUCCESS:
      return {...state, auth: action.auth, id: action.id, token: action.token, role: action.role, name: action.name};
    case userConstants.USERS_LOGIN_FAILURE:
      return {...state, auth: action.auth, error: action.error };
    case userConstants.SET_USER_STATISTIC:
      return {...state, statistic: [...action.list]};
    default:
      return state;
  }
};
