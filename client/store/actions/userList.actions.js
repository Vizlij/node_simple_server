import { userListConstants } from '../../constants';

export const userListActions = {
  addUserList,
  setAllUsers
};

function addUserList(list) {
  return {type: userListConstants.ADD_USERLIST, list}
}

function setAllUsers(list) {
  return {type: userListConstants.SET_ALL_USERS, list}
}
