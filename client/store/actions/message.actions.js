import { messageConstants } from '../../constants';

export const messageActions = {
  addChatHistory,
  addMessage
};

function addChatHistory(chatHistory) {
  return {type: messageConstants.ADD_HISTORY, chatHistory};
}

function addMessage(payload) {
  return {type: messageConstants.ADD_MESSAGE, payload};
}
