import React from 'react';
import {Route, Switch} from 'react-router-dom';
import injectTapEventPlugin from 'react-tap-event-plugin';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Registration from './registration/Registration';
import Chat from './chat/Chat';
import Home from './home/Home';
import Commands from './commands/Commands';
import StatisticsAction from './statisticAction/StatisticAction';
import Users from './users/Users';

injectTapEventPlugin()

export default class Root extends React.Component {

  render() {
    return (
      <MuiThemeProvider>
        <Switch>
          <Route
            exact
            path="/"
            render={props=><Home
              {...props}  />}
          />
          <Route
            exact
            path="/login"
            render={props=><Registration
              {...props}  />}
          />
          <Route
            exact
            path="/chat"
            render={props=><Chat
              {...props}  />}
          />
          <Route
            exact
            path="/commands"
            render={props=><Commands
              {...props} />}
          />
          <Route
            exact
            path="/test-statistics"
            render={props => <StatisticsAction
              {...props} />}
          />
          <Route
            exact
            path="/users"
            render={props => <Users
              {...props} />}
          />
        </Switch>
      </MuiThemeProvider>)
  }
}
