import React from 'react';
import styled from 'styled-components'
import {TextField, SelectField, MenuItem} from 'material-ui';
import {List, ListItem} from 'material-ui/List';
import TabMenu from '../tabMenu/TabMenu';
import './commands.scss';
import {join, leave, unregisterHandler, command} from '../../store/task';
import {connect} from 'react-redux';
import {Scrollable, NoDots, OutputText} from './components/styledComponents';
import shortid from 'shortid';

const ROLE_ADMIN = 1;

class Commands extends React.Component {
  constructor(props, context) {
    super(props, context)

    this.state = {
      chatHistory: [],
      input: '',
      room: null,
      prevRoom: null
    }
  }

  componentWillUnmount() {
    unregisterHandler();
    leave(this.state.selectedValue, this.props.userReducer.name);
  }

  onInput = e => this.setState({input: e.target.value});

  onSendCommands = () => {
    if (!this.state.input)
      return
      command(this.state.selectedValue, this.state.input, (err) => {
      if (err)
        return console.error(err);

      this.updateChatHistory({command: `Command ${this.state.chatHistory.length + 1}`, message: this.state.input });
      return this.setState({input: ''})
    })
  }

  updateChatHistory = entry => this.setState({chatHistory: this.state.chatHistory.concat(entry)})

  handleChange = (event, index, value) => {
    this.setState({prevValue: this.state.room});
    this.setState({
      selectedValue: value,
      chatHistory: []
    }, () => {
      leave(this.state.prevValue, this.props.userReducer.name);
      if (value != ' ')
      join(this.state.selectedValue, {role: this.props.userReducer.role, userName: this.props.userReducer.name}, (err, chatHistory) => {
          if (err)
            console.error(err)
        });
    });
  }

  render() {
    return (<div className='chat'>
      <TabMenu {...this.props}/>
      <div className='workplace'>
        <Scrollable innerRef={(panel) => {
            this.panel = panel;
          }}>
          <List>
            {
                this.state.chatHistory.map(({
                command,
                message,
                event
              }, i) => [<NoDots key={shortid.generate()}>
                <ListItem key={shortid.generate()} primaryText={`${command} ${event || ''}`} secondaryText={message && <OutputText>
                    {message}
                  </OutputText>}/>
              </NoDots>
                ])
            }
          </List>
        </Scrollable>
        <div className='input-block'>
         <SelectField
           floatingLabelText="Users"
           value={this.state.selectedValue}
           onChange={this.handleChange}
           className='users-select'>
                <MenuItem value=' ' primaryText=' '/>
                 {
                  this.props.userListReducer.userList.map(({
                    userName,
                    userEmail,
                    role
                  }, i) => {
                    if(role !== 1) {
                    return <MenuItem value={userName} key={shortid.generate()} primaryText={userName}/>
                  }
                  })
                }
              </SelectField>
          <TextField hintText="Enter a command." floatingLabelText="Enter a command." multiLine={true} rows={4} rowsMax={4} onChange={this.onInput} value={this.state.input} onKeyPress={e => (
              e.key === 'Enter'
              ? this.onSendCommands()
              : null)}/>
        </div>
      </div>
    </div>);
  }
}

export default connect(state => state)(Commands);
