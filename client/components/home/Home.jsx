import React from 'react';
import {connect} from 'react-redux';
import './home.scss';
import moment from 'moment';
import {
  Table,
  TableBody,
  TableFooter,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn
} from 'material-ui/Table';
import {SelectField, MenuItem} from 'material-ui';
import TabMenu from '../tabMenu/TabMenu';
import shortid from 'shortid';
import {userActions} from '../../store/actions';
import config from '../../../config/Configuration';

const ROLE_ADMIN = 1;

class Home extends React.Component {
  state = {
    fixedHeader: true,
    fixedFooter: false,
    stripedRows: false,
    showRowHover: false,
    selectable: true,
    multiSelectable: false,
    enableSelectAll: false,
    deselectOnClickaway: true,
    showCheckboxes: false,
    height: '300px',
    selectedValue: '',
    userList: []
  };

  componentDidMount() {
    if(this.props.userReducer.role !== ROLE_ADMIN && this.props.userReducer.name !== null)
      this.handleChange('','', this.props.userReducer.name);
  }

  handleChange = (event, index, value) => {
    const requestOptions = {
          method: 'GET',
          headers: { 'Content-Type': 'application/json', 'x-access-token': this.props.userReducer.token }
    };
    fetch(`${config.serverURL}api/users/${value}`, requestOptions)
    .then(data => data.json())
    .then(responseJson =>
      this.props.dispatch(userActions.setUserStatistic(responseJson)));
  };

  render() {
    return (
      <div id='home'>
        <TabMenu {...this.props} />
        <Table height={this.state.height} fixedHeader={this.state.fixedHeader} fixedFooter={this.state.fixedFooter} selectable={this.state.selectable} multiSelectable={this.state.multiSelectable}>
          <TableHeader displaySelectAll={this.state.showCheckboxes} adjustForCheckbox={this.state.showCheckboxes} enableSelectAll={this.state.enableSelectAll}>
          <TableRow>
              <TableHeaderColumn tooltip="The Date">Date</TableHeaderColumn>
              <TableHeaderColumn tooltip="The Type">Type</TableHeaderColumn>
              <TableHeaderColumn tooltip="The Message">Message</TableHeaderColumn>
              <TableHeaderColumn tooltip="The Author">Author</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox={this.state.showCheckboxes} deselectOnClickaway={this.state.deselectOnClickaway} showRowHover={this.state.showRowHover} stripedRows={this.state.stripedRows}>
            {
              this.props.userReducer.statistic.map((row, index) => (<TableRow key={shortid.generate()}>
                <TableRowColumn>{row.date}</TableRowColumn>
                <TableRowColumn>{row.type}</TableRowColumn>
                <TableRowColumn>{row.message}</TableRowColumn>
                <TableRowColumn>{row.author}</TableRowColumn>
              </TableRow>))
            }
          </TableBody>
          <TableFooter adjustForCheckbox={this.state.showCheckboxes}>
            <TableRow>
              <TableRowColumn>Date</TableRowColumn>
              <TableRowColumn>Type</TableRowColumn>
              <TableRowColumn>Message</TableRowColumn>
              <TableRowColumn>Author</TableRowColumn>
            </TableRow>
          </TableFooter>
        </Table>
        {this.props.userReducer.role === ROLE_ADMIN &&
          <SelectField
            floatingLabelText="Users"
             value={this.state.selectedValue}
             onChange={this.handleChange}
             className='user-statistic-select'>
               <MenuItem value=' ' primaryText=' '/>
                {
                 this.props.userListReducer.userList.map(({
                   userName,
                   userEmail,
                   role
                 }, i) => {
                   if(role !== ROLE_ADMIN) {
                   return <MenuItem value={userName} key={shortid.generate()} primaryText={userName}/>
                 }
                 })
               }
             </SelectField>}
      </div>
    )
  }
}

export default connect(state => state)(Home);
