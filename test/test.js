var supertest = require("supertest");
var server = supertest.agent("http://localhost:3000");
var io = require('socket.io-client');

describe("HTTP connection to the server", function() {
  it("Try add login without data (get 404 status)", function(done) {
    server.post('/api/auth/login')
    .set('Accept', 'application/json')
    .expect(404, done)
  });

  var token;
  it("Try add login with auth data (get 200 status)", function(done) {
    server.post('/api/auth/login')
    .send({name: 'User', email: 'user@user.com'})
    .set('Accept', 'application/json')
    .expect(200)
    .end((err, res) => {
      token = res.body.token;
      done();
    })
  });

  it("Try set user-action without token key (403)", function(done) {
    server.post('/api/statistic')
    .send({user: 'User', action: 'Test send action from test part of project'})
    .expect(403, done)
  })

  it("Try set user-action with false token key (500)", function(done) {
    server.post('/api/statistic')
    .send({user: 'User', action: 'Test send action from test part of project'})
    .set('x-access-token', 'test')
    .expect(500, done)
  })

  it("Try set user-action with right token key (200)", function(done) {
    server.post('/api/statistic')
    .set('Accept', 'application/json')
    .set('x-access-token', token)
    .send({user: 'User', action: 'Test send action from test part of project'})
    .expect(200, done)
  })

  it("Get user statistic data", function(done) {
    server.get('/api/users/User')
    .set('Accept', 'application/json')
    .set('x-access-token', token)
    .expect(200, done)
  })
});

describe("Socket connection to the server", function() {
  it("Testing socket connection and join/recieving data", function(){
    socket = io.connect('http://localhost:3000/')
    socket.on('connect', function(){
      socket.emit('register', 'User', 'user@user.com')
      socket.emit('join', 'User', {role:2, userName: 'User'})
      socket.emit('message', {
        chatroomName: 'User',
        message: 'Test message',
        user: 'User'
      })
      socket.on("messagerecive", function(data){
        done()
      })
    })
  })
})
