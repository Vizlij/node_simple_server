var mongoose = require('mongoose');
var config = require('../../../config/Configuration');

var LogSchema = new mongoose.Schema({
  name: String,
  move: String,
  lastaction: {
    type: Date,
    default: Date.now
  }
});

mongoose.model(config.db.logTable, LogSchema);

module.exports = mongoose.model(config.db.logTable);
