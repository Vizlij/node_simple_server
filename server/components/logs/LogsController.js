var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');

const mongoose = require('mongoose');

router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());
var Log = require('./Logs');

router.post('/add', function(req, res) {
    Log.create({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        move: req.body.move
    });

    res.status(200).send({status: 200 });
});

module.exports = router;
