var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');

var VerifyToken = require('../VerifyToken');

router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());
var User = require('../user/User');

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var config = require('../../../config/Configuration');

router.post('/login', function(req, res) {
  User.findOne({ name: req.body.name, email: req.body.email }, function (err, user) {
    if (err) return res.status(500).send({status: 500, error: 'Error on the server.'});
    if (!user) return res.status(404).send({status: 404, error: 'No user found.'});

    var token = jwt.sign({ id: user._id }, config.secret, {
      expiresIn: 86400
    });

    User.findByIdAndUpdate(user._id, {$set:{lastvisit: Date.now()}}, function(err, result){
      if(err)
          console.log(err);
      console.log("RESULT: " + result);
  });

    res.status(200).send({status: 200, auth: true, id: user._id, token: token, role: user.role });
  });
});

module.exports = router;
