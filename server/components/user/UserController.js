var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var VerifyToken = require('../VerifyToken');
var moment = require('moment');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

var User = require('./User');
var Message = require('../chat/Message');
var Command = require('../chat/Command');
var Action = require('../statistic/Action');

router.get('/', VerifyToken, function (req, res) {
    User.find({}, function (err, users) {
        if (err) return res.status(500).send({status: 500, error: "There was a problem finding the users."});
        let preparedUsers = [];
        users.forEach((v, i) => {
          preparedUsers.push({
            user: v.name,
            created: prepareDate(v.register),
            lastvisit: prepareDate(v.lastvisit),
            lastaction: prepareDate(v.lastaction)
          });
        });
        res.status(200).send(preparedUsers);
    });
});

router.get('/:user', VerifyToken, function (req, res) {
  User.findOne({'name': req.params.user})
    .then(user => Message.find({$or: [{'sender': user._id}, {'reciever': user._id}]})
    .populate('sender', 'name')
    .populate('reciever', 'name'))
    .then(msgs => {
      const info = [];
      msgs.forEach((v, i) => {
        info.push({
          date: prepareDate(v.date),
          type: 'Message',
          message: v.message,
          author: v.sender.name
        });
      });
      return info;
  })
  .then((info) => {
    return User.findOne({'name': req.params.user})
        .then(user =>
          Action.find({'user': user._id}).populate('user', 'name')
        ).then(actions => {
      actions.forEach((v, i) => {
        info.push({
          date: prepareDate(v.date),
          type: 'Action',
          message: v.action,
          author: v.user.name
        });
      });
      return info;
    })
  }).then((info) => {
    return User.findOne({'name': req.params.user})
        .then(user =>  Command.find({'reciever': user._id}).populate('reciever', 'name')).then(cmd => {
      cmd.forEach((v, i) => {
        info.push({
          date: prepareDate(v.date),
          type: 'Command',
          message: v.command,
          author: 'Admin'
        });
      });
      return info;
    })
  }).then((info) => {
    info.sort(predicateBy('date'));
    res.status(200).send(info);
  });
});

function prepareDate(date) {
  return moment(date).format('dddd Do of MMM YYYY h:mm:ss A');
}

function predicateBy(prop) {
  return function(a, b) {
    if (moment(a[prop], 'dddd Do of MMM YYYY h:mm:ss A') > moment(b[prop], 'dddd Do of MMM YYYY h:mm:ss A')) {
      return 1;
    } else if (moment(a[prop], 'dddd Do of MMM YYYY h:mm:ss A') < moment(b[prop], 'dddd Do of MMM YYYY h:mm:ss A')) {
      return -1;
    }
    return 0;
  }
}

module.exports = router;
