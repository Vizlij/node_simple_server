var mongoose = require('mongoose');
var config = require('../../../config/Configuration');

const commandSchema = {
  _id: mongoose.Schema.Types.ObjectId,
  reciever: {
    type: mongoose.Schema.Types.ObjectId,
    ref: config.db.userTable
  },
  command: String,
  date: {
    type: Date,
    default: Date.now
  }
}

mongoose.model(config.db.commandTable, commandSchema);

module.exports = mongoose.model(config.db.commandTable);
