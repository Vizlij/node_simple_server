var app = require('./app');
var port = 3000;

var server = require('http').createServer(app);
// var io = require('socket.io')(server);

// const makeHandlers = require('./components/chat/Handlers');
// const ChatroomManager = require('./components/chat/ChatroomManager');
// const ClientManager = require('./components/chat/ClientManager');

// const clientManager = ClientManager();
// const chatroomManager = ChatroomManager();

// io.on('connection', function(client) {
//   const {
//     handleRegister,
//     handleJoin,
//     handleLeave,
//     handleMessage,
//     sendUserList,
//     handleCommand
//   } = makeHandlers(client, clientManager, chatroomManager);
//   console.log('client connected...', client.id);

  // client.on('register', handleRegister);
  //
  // client.on('join', handleJoin);
  //
  // client.on('leave', handleLeave);
  //
  // client.on('message', handleMessage);
  //
  // client.on('command', handleCommand);
  //
  // client.on('getuserlist', sendUserList);
  //
  // client.on('disconnect', function() {
  //   console.log('client disconnect...', client.id);
  // });
// });

server.listen(port, function(err) {
  if (err)
    throw err;
  console.log('listening on port ' + port);
});
