var mongoose = require('mongoose');
var db = mongoose.connect(`mongodb://localhost:27017/node-test`, { useMongoClient: true });
var moment = require('moment');

var User = require('../../components/user/User');
var Action = require('../../components/statistic/Action');
var Message = require('../../components/chat/Message');
var Command = require('../../components/chat/Command');

User.create({
  _id: new mongoose.Types.ObjectId(),
  name: "Admin",
  email: "admin@admin.com",
  role: "1",
  register: moment(),
  lastvisit: moment(),
  lastaction: moment()
}).then(() => {
  User.create({
    _id: new mongoose.Types.ObjectId(),
    name: "User",
    email: "user@user.com",
    role: "2",
    register: moment(),
    lastvisit: moment(),
    lastaction: moment()
  }).then(() => {
    mongoose.disconnect();
  });
});
