Install.

Import database from node-test.

``` bash
npm install
```

Run the socket server.

``` bash
npm run server
```

Run dev server, clients can be requested on localhost:3001.

``` bash
npm run client
```
For test app exist users:
  admin-user (name: Admin, email: admin@admin.com)
  user (name: User, email: user@user.com)


Start Test

$ mocha test
